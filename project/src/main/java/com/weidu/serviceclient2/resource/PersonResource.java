package com.weidu.serviceclient2.resource;

import com.weidu.serviceclient2.api.rest.PersonApi;
import com.weidu.serviceclient2.entity.Person;

/**
 * Created by chen on 18/5/23.
 */

public class PersonResource implements PersonApi{
    private Person person = new Person("陈鹏 from hello2", 17);
    @Override
    public Person getOne() {
        return person;
    }

    @Override
    public String getName() {
        return person.getName();
    }
}
