package com.weidu.serviceclient2.controller;

import com.weidu.serviceclient2.api.springmvc.UserApi;
import com.weidu.serviceclient2.entity.User;
import org.springframework.stereotype.Controller;

/**
 * Created by chen on 18/5/23.
 */
@Controller
public class UserController implements UserApi{

    private User user = new User(1, "chenpengAccount");

    @Override
    public User getOne() {
        return user;
    }

    @Override
    public String getAccount() {
        return user.getAccount();
    }
}
