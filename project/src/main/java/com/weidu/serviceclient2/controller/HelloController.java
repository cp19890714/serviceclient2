package com.weidu.serviceclient2.controller;

import com.weidu.serviceclient1.api.rest.AuthorApi;
import com.weidu.serviceclient1.api.springmvc.ArticleApi;
import com.weidu.serviceclient1.entity.Article;
import com.weidu.serviceclient1.entity.Author;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.netflix.eureka.serviceregistry.EurekaRegistration;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by chen on 18/5/21.
 */
@RestController
@RequestMapping("/hello")
public class HelloController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private EurekaRegistration eurekaRegistration;

    @GetMapping("say")
    public String index(){
        /*ServiceInstance serviceInstance = eurekaRegistration.g;
        logger.info("/hello, host:{}, port:{} ", serviceInstance.getHost(), serviceInstance.getPort(),
                serviceInstance.getServiceId(), serviceInstance.getUri(), serviceInstance.getServiceId());*/
        logger.info("/hello, host:{}, port:{} ",eurekaRegistration.getHost(), eurekaRegistration.getPort());
        return "hello world";
    }

    @Autowired
    private AuthorApi authorApi;

    @RequestMapping("/author")
    public Author getAuthor(){
        return authorApi.getOne();
    }

    @RequestMapping("/author/realname")
    public String getAuthorName(){
        return authorApi.name();
    }

    @Autowired
    private ArticleApi articleApi;
    @RequestMapping("/article")
    public Article getArticle(){
        return articleApi.getOne();
    }

    @RequestMapping("/article/title")
    public String getArticleName(){
        return articleApi.getTitle();
    }

}
