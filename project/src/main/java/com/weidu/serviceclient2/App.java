package com.weidu.serviceclient2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.WebMvcRegistrations;
import org.springframework.boot.autoconfigure.web.WebMvcRegistrationsAdapter;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.core.annotation.AnnotatedElementUtils;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

/**
 * Hello world!
 *
 */
@SpringBootApplication
@EnableDiscoveryClient
@ComponentScan(basePackages = "com.weidu", excludeFilters = {@ComponentScan.Filter(type=FilterType.REGEX, pattern = "com.weidu..*FeignConfiguration")})
@EnableFeignClients(value = "com.weidu.*.api")
public class App
{
    public static void main( String[] args )
    {
        SpringApplication.run(App.class, args);
    }

    /**
     * 解决同时有@FeignClien、@RequestMapping的接口,会被加载到请求映射中 的问题
     * 或者可以通过排除扫描路径来解决
     * @return
     */
    @Bean
    public WebMvcRegistrations feignWebRegistrations() {
        return new WebMvcRegistrationsAdapter() {
            @Override
            public RequestMappingHandlerMapping getRequestMappingHandlerMapping() {
                return new FeignRequestMappingHandlerMapping();
            }
        };
    }

    private static class FeignRequestMappingHandlerMapping extends RequestMappingHandlerMapping {
        @Override
        protected boolean isHandler(Class<?> beanType) {
            return super.isHandler(beanType) && !(beanType.isInterface() && AnnotatedElementUtils.hasAnnotation(beanType, FeignClient.class));
        }
    }
}
