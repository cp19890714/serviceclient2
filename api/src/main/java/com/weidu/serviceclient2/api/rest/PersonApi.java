package com.weidu.serviceclient2.api.rest;

import com.weidu.framework.feign.JAXRSFeignConfiguration;
import com.weidu.serviceclient2.entity.Person;
import org.springframework.cloud.netflix.feign.FeignClient;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Created by chen on 18/5/21.
 */
@FeignClient(name="hello2rest", path="${hello2.restprefix}", url="${hello2.name}", configuration = JAXRSFeignConfiguration.class)
@Path("person")
public interface PersonApi {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    Person getOne();

    @GET
    @Path("name")
    String getName();
}
