package com.weidu.serviceclient2.api.springmvc;

import com.weidu.framework.feign.SpringMVCFeignConfiguration;
import com.weidu.serviceclient2.entity.User;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by chen on 18/5/23.
 */
@FeignClient(name="hello2springmvc", url="${hello2.name}", configuration = SpringMVCFeignConfiguration.class)
@RequestMapping("user")
public interface UserApi {

    @RequestMapping("getone")
    @ResponseBody
    public User getOne();

    @RequestMapping("/account")
    @ResponseBody
    public String getAccount();
}
