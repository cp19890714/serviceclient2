package com.weidu.serviceclient2.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * Created by chen on 18/5/23.
 */
@Configuration
@PropertySource(value= "com/weidu/serviceclient2/config/apiconfig.properties")
public class Hello2ApiConfig {

}
