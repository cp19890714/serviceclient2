package com.weidu.serviceclient2.entity;

/**
 * Created by chen on 18/5/23.
 */
public class User {
    private Integer id;
    private String account;

    public User(Integer id, String account) {
        this.id = id;
        this.account = account;
    }

    public User() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }
}
